# Beer-bizcuit-example

## Getting started
   This is a job interview question, and the topic has been divided into three separate questions for your convenience.
 
* Question 1: Basic coding competency check:
    * To access this question, please navigate to the `basic-coding-competency` branch using the command `git checkout basic-coding-competency`
* Question 2: NodeJS Competency Test:
    * To access this question, please navigate to the `node-js-competency` branch using the command `git checkout node-js-competency`
* Question 3: React Competency Test:
    * To access this question, please navigate to the `react-competency-test` branch using the command `git checkout react-competency-test`


****
Please note that if you encounter any issues or have any questions, feel free to reach out to us. We apologize for any inconvenience caused by any errors or lack of information in this README.
****

